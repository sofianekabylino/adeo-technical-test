package adeo.leroymerlin.cdp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventRepositoryTest {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void find_event_should_retrieve_it_from_database() {
        Event savedEvent = entityManager.persist(new Event());
        assertThat(eventRepository.findById(savedEvent.getId()).orElse(null)).isNotNull();
    }

    @Test
    public void find_event_should_retrieve_nothing() {
        Event event = new Event();
        event.setId(1L);

        assertThat(eventRepository.findById(event.getId()).orElse(null)).isNull();
    }

    @Test
    public void find_all_should_retrieve_all_events_from_database() {
        Event event = new Event();
        Event event2 = new Event();

        Event savedEvent = entityManager.persist(event);
        Event savedEvent2 = entityManager.persist(event2);
        assertThat(eventRepository.findAllBy()).contains(savedEvent, savedEvent2);
    }

    @Test
    public void save_event_should_add_it_in_database() {
        Event event = new Event();
        event.setTitle("Test Title");

        Event savedEvent = eventRepository.save(event);
        assertThat(savedEvent).isNotNull();
        assertThat(savedEvent.getTitle()).isEqualTo("Test Title");
    }

    @Test
    public void delete_event_should_remove_it_from_database() {
        Event event = new Event();
        Event savedEvent = entityManager.persist(event);
        assertThat(savedEvent).isNotNull();

        eventRepository.delete(savedEvent.getId());
        Event deletedEvent = eventRepository.findById(savedEvent.getId()).orElse(null);

        assertThat(deletedEvent).isNull();
    }
}
