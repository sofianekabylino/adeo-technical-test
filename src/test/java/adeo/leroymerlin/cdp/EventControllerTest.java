package adeo.leroymerlin.cdp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(EventController.class)
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    @Test
    public void update_event_should_be_ok() throws Exception {
        Event event = new Event();
        event.setId(1L);
        event.setComment("Test Comment");

        doNothing().when(eventService).updateEvent(anyLong(), any(Event.class));
        when(eventService.getEvent(anyLong())).thenReturn(event);

        mockMvc.perform(put("/api/events/1")
                        .content(new ObjectMapper().writeValueAsString(event))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        verify(eventService, times(1)).updateEvent(anyLong(), any(Event.class));

    }

    @Test
    public void delete_event_should_be_ok() throws Exception {
        Event event = new Event();
        event.setId(1L);

        when(eventService.getEvent(anyLong())).thenReturn(event);
        doNothing().when(eventService).delete(anyLong());

        mockMvc.perform(delete("/api/events/1"))
                .andExpect(status().isOk());

        verify(eventService, times(1)).delete(anyLong());
    }

    @Test
    public void find_events_should_be_ok() throws Exception {
        Event event = new Event();
        event.setId(1L);
        Event event2 = new Event();
        event2.setId(2L);

        when(eventService.getEvents()).thenReturn(Arrays.asList(event, event2));

        mockMvc.perform(get("/api/events/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[1].id").value("2"));

    }

    @Test
    public void search_event_with_query_should_be_ok() throws Exception {

        Event event = new Event();
        event.setTitle("Alcatraz fest");
        Member member = new Member();
        member.setName("Queen Robbie Bender");
        Member member2 = new Member();
        member2.setName("Queen Laila Shelton");
        Member member3 = new Member();
        member3.setName("Queen Eleanor Fisher (Ellie)");
        Member member4 = new Member();
        member4.setName("Queen Kimberly Jacobs");
        Member member5 = new Member();
        member5.setName("Queen Abigail Cardenas");
        Band band = new Band();
        band.setName("Megadeth");
        Band band2 = new Band();
        band2.setName("AC/DC");
        band.setMembers(new HashSet<>(Arrays.asList(member, member2, member3)));
        band2.setMembers(new HashSet<>(Arrays.asList(member4, member5)));
        event.setBands(new HashSet<>(Arrays.asList(band, band2)));

        event.setTitle("Alcatraz fest [2]");

        when(eventService.getFilteredEvents(anyString())).thenReturn(Collections.singletonList(event));

        mockMvc.perform(get("/api/events/search/Rob"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("Alcatraz fest [2]"));

    }
}
