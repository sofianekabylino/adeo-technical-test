package adeo.leroymerlin.cdp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceTest {

    @InjectMocks
    private EventService eventService;
    @Mock
    private EventRepository eventRepository;

    @Test
    public void update_event_should_be_ok(){
        Event event = new Event();
        when(eventRepository.findById(anyLong())).thenReturn(Optional.of(event));
        when(eventRepository.save(any(Event.class))).thenReturn(event);
        eventService.updateEvent(1L, event);
        verify(eventRepository, times(1)).save(any(Event.class));;
    }

    @Test
    public void get_event_with_id_should_retrieve_event(){
        Event event = new Event();
        when(eventRepository.findById(anyLong())).thenReturn(Optional.of(event));

        assertThat(eventService.getEvent(1L)).isEqualTo(event);
    }

    @Test
    public void get_event_with_id_should_not_retrieve_event(){
        when(eventRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThat(eventService.getEvent(1L)).isNull();
    }

    @Test
    public void get_all_events_should_retrieve_all_events(){
        Event event = new Event();
        Event event2 = new Event();
        when(eventRepository.findAllBy()).thenReturn(Arrays.asList(event, event2));

        assertThat(eventService.getEvents()).contains(event, event2);
    }

    @Test
    public void delete_event_should_be_ok(){
        Event event = new Event();
        event.setId(1L);

        doNothing().when(eventRepository).delete(anyLong());

        eventService.delete(event.getId());

        verify(eventRepository, times(1)).delete(anyLong());
    }

    @Test
    public void filter_with_query_should_return_event(){
        Event event = new Event();
        event.setTitle("Alcatraz fest");
        Member member = new Member();
        member.setName("Queen Robbie Bender");
        Member member2 = new Member();
        member2.setName("Queen Laila Shelton");
        Member member3 = new Member();
        member3.setName("Queen Eleanor Fisher (Ellie)");
        Member member4 = new Member();
        member4.setName("Queen Kimberly Jacobs");
        Member member5 = new Member();
        member5.setName("Queen Abigail Cardenas");
        Band band = new Band();
        band.setName("Megadeth");
        Band band2 = new Band();
        band2.setName("AC/DC");
        band.setMembers(new HashSet<>(Arrays.asList(member, member2, member3)));
        band2.setMembers(new HashSet<>(Arrays.asList(member4, member5)));
        event.setBands(new HashSet<>(Arrays.asList(band, band2)));

        when(eventRepository.findAllBy()).thenReturn(Collections.singletonList(event));

        List<Event> filteredEvents = eventService.getFilteredEvents("Robbie");
        assertThat(filteredEvents).isNotEmpty();
        assertThat(filteredEvents.get(0).getTitle()).isEqualTo("Alcatraz fest [2]");

    }

    @Test
    public void filter_with_query_should_return_null(){
        Event event = new Event();
        event.setTitle("Alcatraz fest");
        Member member = new Member();
        member.setName("Queen Robbie Bender");
        Member member2 = new Member();
        member2.setName("Queen Laila Shelton");
        Member member3 = new Member();
        member3.setName("Queen Eleanor Fisher (Ellie)");
        Member member4 = new Member();
        member4.setName("Queen Kimberly Jacobs");
        Member member5 = new Member();
        member5.setName("Queen Abigail Cardenas");
        Band band = new Band();
        band.setName("Megadeth");
        Band band2 = new Band();
        band2.setName("AC/DC");
        band.setMembers(new HashSet<>(Arrays.asList(member, member2, member3)));
        band2.setMembers(new HashSet<>(Arrays.asList(member4, member5)));
        event.setBands(new HashSet<>(Arrays.asList(band, band2)));

        when(eventRepository.findAllBy()).thenReturn(Collections.singletonList(event));

        assertThat(eventService.getFilteredEvents("Test")).isEmpty();

    }
}
