package adeo.leroymerlin.cdp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> getEvents() {
        return eventRepository.findAllBy();
    }

    public Event getEvent(Long id) {
        return eventRepository.findById(id).orElse(null);
    }

    public void updateEvent(Long id, Event event) {
        Optional<Event> eventToUpdate = eventRepository.findById(id);
        if (eventToUpdate.isPresent()) {
            eventRepository.save(event);
        }
    }

    public void delete(Long id) {
        eventRepository.delete(id);
    }

    public List<Event> getFilteredEvents(String query) {
        List<Event> events = eventRepository.findAllBy();
        // Filter the events list in pure JAVA here
        List<Event> filteredEvents = events.stream()
                .filter(
                        event -> event.getBands().stream()
                                .flatMap(band -> band.getMembers().stream())
                                .anyMatch(member -> member
                                        .getName().toLowerCase()
                                        .contains(query.toLowerCase())
                                )
                )
                .collect(Collectors.toList());
        return addChildCountsToParents(filteredEvents);
    }

    private static List<Event> addChildCountsToParents(List<Event> filteredEvents) {
        return filteredEvents.stream().peek(event -> {
            event.setTitle(String.format(event.getTitle() + " [%d]", event.getBands().size()));
            event.getBands().forEach(band -> band.setName(String.format(band.getName() + " [%d]", band.getMembers().size())));
        }).collect(Collectors.toList());
    }
}
