package adeo.leroymerlin.cdp;

import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface EventRepository extends Repository<Event, Long> {

    Optional<Event> findById(Long id);

    Event save(Event event);
    void delete(Long eventId);
    List<Event> findAllBy();
}
