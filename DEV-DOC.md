## Adeo Test Dev Documentation

## Adding Review and Notes
* Initially, I manually tested the feature to ensure that it's not working as expected.
* Since the front-end appears to be functioning correctly, I investigated the method (entry point) responsible for updating events (`EventController.updateEvent(...)`).
* Discovered that the method was empty.
* Implemented the `findById(Long id)` method in the `EventRepository` interface.
* Removed `readOnly=true` from the `@Transactional` annotation in the `EventRepository` interface.
* Added `@Transient` to the relation with the `bands` property in the `Event` class to ignore actions on it.

## Removing Event
* Removed `readOnly=true` from the `@Transactional` annotation.
* Removed the `@Transient` annotation in the `Event` class and added the `cascade type ALL` to the `@OneToMany` annotation to apply actions on the associated entity.
* Applied the same changes in the `Band` class for the relation with members.

## Filtering with Endpoint `/search/{query}`
* Utilized the Stream API.
* Based on the existing method, which retrieves all events, applied filtering on the list by extracting bands and transforming their list of members into one simple list, encompassing all the members. This approach avoids looping through brands.
* Converted names to lowercase for increased flexibility.

## Bonus
* To add the count of children in event title and band name, utilized the `peek()` method of the Stream API on every event of the previous result.
* In the `peek` method, modified the title to include the count, and for each band, transformed the name using the `forEach` method.
* Opted for `peek` instead of `map` as it doesn't transform each element of the stream and produce a stream of transformed values.

## Tests
* Added tests for the entire project (repository, service, and controller).
* Utilized MockMvc to test controllers.
* Employed `@DataJpaTest` for the repository tests.
* Used `SpringRunner.class` for controller and repository tests to initialize the Spring context.
* To test the service class, I used MockitoJUnitRunner, Mocks and InjectMocks